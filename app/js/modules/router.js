/* global Content */
import { drawCategory, drawProject } from '../main.js'

var Router = {
  getUrl: function () {
    var rawPath = window.location.pathname.split('/'),
      path = [];

    for (var i = 0; i < rawPath.length; i++) {
      if (rawPath[i].length > 0) {
        path.push(rawPath[i])
      }
    }

    return { module: path[0] || 'all', page: path[1] }
  },

  init: function () {
    var path = this.getUrl();

    if (path.page) {
      drawProject(path.page)
    } else if (path.module) {
      drawCategory(Content.getCategoryId(path.module));
    }
  }
}

export { Router }
