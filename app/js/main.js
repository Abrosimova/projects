/* global Content, Navigo */
var router = new Navigo('http://ad.dnevnik.ru/promo/projects-new');

$(document).ready(function() {
  var element,
    scroll;

  router
  .on({
    '*': function (params, query) {
      if (query.indexOf('section') > -1) {
        query = query.split('=');

        drawCategory(query[1]);
        if (element) {
          $('html, body').animate({
            scrollTop: element.offset().top + scroll
          }, 1000);
          element = null;
          scroll = 0;
        }
      } else if (query.indexOf('project') > -1) {
        query = query.split('=')
        drawProject(query[1])
      } else {
        router.navigate('?section=1', true);
      }
    }
  })
  .notFound(function() {
    router.navigate('?section=1', true);
  })
  .resolve(window.location.href);

  $('.header__logo').on('click', function() {
    element = null;
    $('.main-page').removeClass('main-page--inner');
    $('.header').removeClass('header--inner');
    drawMain();
    router.navigate('?section=1', true);
  })

  $('.main-page').on('click', '.menu-item', function () {
    if ($(this).hasClass('menu-item--active')) {
      return
    }
    router.navigate('?section=' + $(this).data('id'), true);
  });

  $('.main-page').on('click', '.project', function () {
    element = $(this);
    scroll = $(window).scrollTop();
    if ($(this).parent().hasClass('no-page')) {
      return
    }
    router.navigate('?project=' + $(this).data('id'), true);
  })

  $('.main-page').on('click', '.back-link', function () {
    $('.main-page').removeClass('main-page--inner');
    $('.header').removeClass('header--inner');
    drawMain();
    history.back();
    router.navigate(window.location.href, true);
  })
})

function drawMain() {
  var html = '';

  html += '<div class="main-title">Спецпроекты на Дневник.ру </div>'
  html += '<ul class="menu">'
  html += '<li class="menu-item menu-item--active" data-id="1">Все</li>'
  html += '<li class="menu-item" data-id="2">Электроника, телеком, IT </li>'
  html += '<li class="menu-item" data-id="3">Кино, мультфильмы</li>'
  html += '<li class="menu-item" data-id="4">Товары и услуги </li>'
  html += '<li class="menu-item" data-id="5">Социальные проекты</li>'
  html += '<li class="menu-item" data-id="6">Фармацевтика</li>'
  html += '<li class="menu-item" data-id="7">Еда, напитки</li>'
  html += '<li class="menu-item" data-id="8">Образование</li>'
  html += '</ul>'
  html += '<div class="projects-container">'
  html += '</div>'
  html += '<div class="contacts">'
  html += '<div class="contacts__title">Понравились наши проекты?</div>'
  html += '<div class="contacts__item">'
  html += '<div class="contacts__item-name">По вопросам сотрудничества пишите нам на нашу почту:  </div>'
  html += '<div class="contacts__item-value contacts__item-value--a" '
  html += 'href="mailto:adv@dnevnik.ru">adv@dnevnik.ru</div>'
  html += '</div>'
  html += '<div class="contacts__item">'
  html += '<div class="contacts__item-name">Или звоните по телефону:</div>'
  html += '<div class="contacts__item-value">8 (804) 333-55-06</div>'
  html += '</div>'
  html += '</div>'
  $('.main-page').empty().html(html);
}

function drawCategory(id) {
  var data = Content.getCategory(id),
    html = '',
    i;

  id = id|0;
  $('.menu-item--active').removeClass('menu-item--active');
  $('.menu-item[data-id="' + id + '"]').addClass('menu-item--active');
  if (id === 9) {
    for (i = 0; i < data.length; i++) {
      html += data[i];
    }
    $('.projects-container').empty().html(html);
    var $grid = $('.category-grid').masonry({
      itemSelector: '.grid-item',
      percentPosition: true,
      columnWidth: '.category-grid__sizer',
      // fitWidth: true
    });

    // layout Masonry after each image loads
    $grid.imagesLoaded().progress( function() {
      $grid.masonry();
    });

    return;
  }
  for (i = 0; i < data.length; i++) {
    // if (data[i].noOpen === 1) {
    //   html += '<a href="' + data[i].link + '" target="_blank" class="no-page">'
    // }
    html += '<div class="project" data-id="' + data[i].id + '">'
    html += '<img src="' + data[i].img + '" alt="" class="project__img" width="450" height="280">'
    html += '<div class="project__brand">' + data[i].brand + '</div>'
    html += '<div class="project__product">' + data[i].product + '</div>'
    html += '<div class="project__name">' + data[i].name + '</div>'
    html += '</div>'
    // if (data[i].noOpen === 1) {
    //   html += '</a>'
    // }
  }
  $('.projects-container').empty().html(html);
}

function drawProject(id) {
  var data = Content.getProject(id),
    html = '';

  html += '<div class="inner-project-desc">';
  html += '<div class="back-link">< Назад</div>'
  html += '<div class="inner-project__vendor">' + data.brand + '</div>'
  html += '<div class="inner-project__product">' + data.product + '</div>'
  html += '<div class="inner-project__name">' + data.name + '</div>'
  html += '<div class="inner-project__subtitle">Описание: </div>'
  html += '<div class="inner-project__desc">' + data.desc + '</div>'
  if (data.content.length > 0) {
    html += '<div class="inner-project__subtitle">Состав проекта:</div>'
    html += '<ul class="inner-project__ul">'
    for (var i = 0; i < data.content.length; i++) {
      html += '<li><span>' + data.content[i] + '</span></li>'
    }
    html += '</ul>'
  }
  if (Array.isArray(data.link)) {
    for (i = 0; i < data.link.length; i++) {
      html += '<a href="' + data.link[i].link + '" target="_blank" class="menu-item menu-item--active project-link">'
      html += data.link[i].name + '</a>'
    }
  } else {
    html += '<a href="' + data.link + '" target="_blank" class="menu-item menu-item--active project-link">'
    html += 'Посмотреть проект</a>'
  }
  html += '</div>'
  html += '<div class="inner-project-img grid">';
  html += '<div class="grid-sizer"></div>'
  for (i = 0; i < data.largeImg.length; i++) {
    html += '<div class="grid-item ' + (data.largeImg[i].size === 1 ? ' item-size--large' : '') + '">'
    html += '<img class="inner-project-img__img" src="' + data.largeImg[i].img + '">'
    html += '</div>'
  }
  html += '</div>'
  $('.main-page').addClass('main-page--inner');
  $('.header').addClass('header--inner');
  $('.main-page').empty().html(html);
  window.scrollTo(0, 0);

  if ($('.inner-project-desc').height() + 220 <= document.documentElement.clientHeight &&
    document.documentElement.clientWidth > 1350) {
    $('.inner-project-desc').css({ position: 'fixed', top: '130px', left: '20px' })
  }

  var $grid = $('.grid').masonry({
    itemSelector: '.grid-item',
    percentPosition: true,
    columnWidth: '.grid-sizer',
    // fitWidth: true
  });

  // layout Masonry after each image loads
  $grid.imagesLoaded().progress( function() {
    $grid.masonry();
  });
}
